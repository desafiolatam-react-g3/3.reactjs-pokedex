EJERCICIO 3: POKEDEX
---
Se debe desarrollar un PokeDex que entregue información de la mayoría de los pokémon existentes.

La aplicación debe mostrar una lista de tarjetas, en donde cada tarjeta representa un pokémon.

Las tarjetas deben mostrar la imagen y nombre del pokémon que representan, y al ser presionadas, deben levantar una ventana modal que despliegue las características de dicho pokémon.


### Fuente de datos
- Lista de Pokémon: `http://pokeapi.co/api/v2/pokemon?limit=811`
- Detalles de Pokémon: `http://pokeapi.co/api/v2/pokemon/{:id}/`

## Recursos
- [Documentación oficial de PokeAPI](https://pokeapi.co/docsv2)
- [Documentación oficial de Reactjs](https://reactjs.org/docs)
- [Documentación oficial de Reduxjs](https://redux.js.org/)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
